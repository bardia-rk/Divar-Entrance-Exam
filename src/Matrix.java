import java.util.*;

public class Matrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> numbers = new ArrayList<>();
        int m = scanner.nextInt();
        int[][] matrix = new int[m][m];

        for (int i = 0; i < m; i++)
            for (int j = 0; j < m; j++)
                matrix[i][j] = scanner.nextInt();

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                if (i + j + 1 == m && i == j && check(matrix[i][j]))
                    numbers.add(matrix[i][j]);
                if (i + j + 1 == m || i == j)
                    if (check(matrix[i][j]))
                        numbers.add(matrix[i][j]);
            }
        }
        int sum = 0;
        for (Integer number : numbers) sum += number;
        System.out.println(sum);
    }

    public static boolean check(int number) {
        return (number - 1) % 3 == 0;
    }
}
