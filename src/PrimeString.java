import java.util.Scanner;

public class PrimeString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String in = scanner.nextLine();
        for (int i = 0; i < in.length() - 1; i++) {
            if (!Character.isDigit(in.charAt(i)) || !Character.isDigit(in.charAt(i + 1))) continue;
            else {
                int number = Integer.parseInt(String.valueOf(in.charAt(i)) + (in.charAt(i + 1)));
                if (isPrime(number)) System.out.println(number);
            }
        }
    }

    private static boolean isPrime(int number) {
        if (number < 10 || number > 99) return false;
        for (int i = 2; i < number; i++)
            if (number % i == 0) return false;
        return true;
    }
}
