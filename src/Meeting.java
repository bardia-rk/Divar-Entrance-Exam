import java.util.Scanner;

public class Meeting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean flag = false;
        int[][] calendar = new int[n][3];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < 3; j++)
                calendar[i][j] = scanner.nextInt();
        scanner.nextInt();
        int length = scanner.nextInt();

        if (length < calendar[0][1]) {
            System.out.println("0 " + length);
            flag = true;
        } else
            for (int i = 0; i < n - 1; i++) {
                if (length < calendar[i + 1][1] - calendar[i][2] - 1) {
                    System.out.println((calendar[i][2] + 1) + " " + (calendar[i][2] + 1 + length));
                    flag = true;
                    break;
                }
            }
        if (!flag) {
            System.out.println((calendar[n - 1][2] + 1) + " " + (calendar[n - 1][2] + 1 + length));
        }
    }
}
